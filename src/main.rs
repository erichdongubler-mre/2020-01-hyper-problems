use {
    futures::future::{self, ok},
    hyper::{
        client::{
            connect::{Connected, Connection},
            Client,
        },
        service::Service,
        Body, Uri,
    },
    log::LevelFilter,
    std::{
        cmp::min,
        io,
        pin::Pin,
        task::{Context, Poll, Waker},
    },
    tokio::io::{AsyncRead, AsyncWrite},
};

pub struct MockPollStream {
    data: Vec<u8>,
    pos: usize,
    ready_for_response: bool,
    waker: Option<Waker>,
}

impl MockPollStream {
    pub fn new(data: Vec<u8>) -> Self {
        Self {
            data,
            pos: 0,
            ready_for_response: false,
            waker: None,
        }
    }
}

impl AsyncRead for MockPollStream {
    fn poll_read(
        mut self: Pin<&mut Self>,
        cx: &mut Context,
        buf: &mut [u8],
    ) -> Poll<io::Result<usize>> {
        self.waker = Some(cx.waker().clone());
        if !self.ready_for_response {
            return Poll::Pending;
        }
        let n = min(buf.len(), self.data.len() - self.pos);
        let read_until = self.pos + n;
        buf[..n].copy_from_slice(&self.data[self.pos..read_until]);
        self.pos = read_until;
        Poll::Ready(Ok(n))
    }
}

impl AsyncWrite for MockPollStream {
    fn poll_write(
        self: Pin<&mut Self>,
        _cx: &mut Context,
        _data: &[u8],
    ) -> Poll<io::Result<usize>> {
        let Self {
            data,
            ready_for_response,
            waker,
            ..
        } = self.get_mut();
        *ready_for_response = true;
        if let Some(w) = waker.take() {
            w.wake();
        }
        Poll::Ready(Ok(data.len()))
    }

    fn poll_flush(self: Pin<&mut Self>, _cx: &mut Context) -> Poll<io::Result<()>> {
        Poll::Ready(Ok(()))
    }

    fn poll_shutdown(self: Pin<&mut Self>, _cx: &mut Context) -> Poll<io::Result<()>> {
        Poll::Ready(Ok(()))
    }
}

impl Connection for MockPollStream {
    fn connected(&self) -> Connected {
        Connected::new()
    }
}

#[derive(Default, Clone)]
pub struct DummyConnector;

impl Service<Uri> for DummyConnector {
    type Response = MockPollStream;
    type Error = std::io::Error;
    type Future = future::Ready<Result<Self::Response, Self::Error>>;

    fn poll_ready(&mut self, _cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        Poll::Ready(Ok(()))
    }

    fn call(&mut self, _req: Uri) -> Self::Future {
        ok(MockPollStream::new("asdf".to_owned().into_bytes()))
    }
}

#[tokio::main]
async fn main() {
    env_logger::builder()
        .filter_level(LevelFilter::Trace)
        .init();
    let client = Client::builder().build::<_, Body>(DummyConnector::default());
    let _ = client
        .get("http://127.0.0.1".parse().unwrap())
        .await
        .unwrap();
}
